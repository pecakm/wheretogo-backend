const ENV_VARS = require('../../env_vars');

const CONSTANTS = {
    DATABASE_URL: ENV_VARS.DATABASE_URL,
    TOKEN_EXP: 3600, // 1 hour

    PLACES_LIMIT: 20,
    DISTANCE_MULTIPLIER: 0.001, // convert meters to kilometers
};

module.exports = CONSTANTS;