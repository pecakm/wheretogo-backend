const mongoose = require('mongoose');
const Activity = require('../models/activity');
const PlacesController = require('../controllers/places-controller');

exports.like = (req, res, next) => {
    Activity.findById(req.params.activityId, 'likes').then(activity => {
        addLike(activity, req, res);
    });
};

function addLike(activity, req, res) {
    activity.likes.push(req.userData.id);

    activity.save().then(updatedActivity => {
        res.status(200).json(updatedActivity.likes.length);
    }).catch(error => {
        res.json(500).json({ error: error });
    });
}

exports.addNew = (req, res, next) => {
    let activity = new Activity({
        _id: new mongoose.Types.ObjectId,
        place: mongoose.Types.ObjectId(req.body.placeId),
        date: Date.now(),
        text: req.body.text
    });

    if (req.body.authorId) {
        activity.author = mongoose.Types.ObjectId(req.body.authorId);
    }

    if (req.file) {
        activity.imageUrl = req.file.path;
    }

    activity.save().then(addedActivity => {
        PlacesController.addActivityToPlace(addedActivity, res);
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};
