const mongoose = require('mongoose');

const CONSTANTS = require('../helpers/constants');
const Place = require('../models/place');
const UsersController = require('../controllers/users-controller');

exports.getDetailLogged = (req, res, next) => {
    this.getDetail(req, res, next);
};

exports.getDetail = (req, res, next) => {
    Place.findById(req.params.placeId).populate({
        path: 'activities',
        options: {
            sort: {
                date: -1
            }
        },
        populate: {
            path: 'author',
            select: 'username'
        }
    }).then(place => {    
        res.status(200).json(Place.createPlaceDetailResponse(place, req));
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

exports.getPlaces = (req, res, next) => {
    Place.aggregate([{
        $geoNear: {
            query: {
                $and: [
                    { category: { $ne: 'route' } },
                    { category: { $ne: 'street_address' } },
                    { category: { $ne: 'locality' } }
                ]
            },
            near: {
                type: 'Point',
                coordinates: [Number(req.query.lng), Number(req.query.lat)]
            },
            distanceField: 'distance',
            $limit: CONSTANTS.PLACES_LIMIT,
            distanceMultiplier: CONSTANTS.DISTANCE_MULTIPLIER,
            spherical: true
        }
    }]).project(Place.markerInfo()).then(places => {
        res.status(200).json(Place.createPlacesResponse(places));
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

exports.findPlace = (req, res, next) => {
    Place.findOne({
        location: req.body.location
    }).then(place => {
        if (place) {
            res.status(200).json(Place.createCurrentPlaceResponse(place));
        } else {
            saveNewPlace(req, res);
        }
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

function saveNewPlace(req, res) {
    let place = new Place(req.body);
    place._id = new mongoose.Types.ObjectId;
    
    place.save().then(addedPlace => {
        res.status(201).json(Place.createCurrentPlaceResponse(addedPlace));
    }).catch(error => {
        res.status(500).json({ error: error });
    });
}

exports.addActivityToPlace = (activity, res) => {
    Place.findById(activity.place, 'activities').then(place => {
        place.activities.push(activity);
        
        place.save().then(updatedPlace => {
            UsersController.addActivityToProfile(activity, res);
        }).catch(error => {
            res.status(500).json({ error: error });
        });
    }).catch(error => {
        res.status(500).json({ error: error });
    });
}
