const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const ENV_VARS = require('../../env_vars');
const CONSTANTS = require('../helpers/constants');
const User = require('../models/user');

exports.getMyAccount = (req, res, next) => {
    User.findById(req.userData.id).then(user => {
        res.status(200).json(User.createMyAccountResponse(user));
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

exports.getUser = (req, res, next) => {
    User.findById(req.params.userId).populate({
        path: 'activities',
        options: {
            sort: {
                date: -1
            }
        },
        populate: {
            path: 'author',
            select: 'username'
        }
    }).then(user => {
        res.status(200).json(User.createProfileResponse(user, req));
    }).catch(error => {
        res.status(500).json({ error: error });
    })
}

exports.signup = (req, res, next) => {
    if (!validPassword(req.body.password)) {
        return res.status(422).json({ message: 'Cannot use this password' });
    }

    User.findOne({
        email: req.body.email
    }).then(user => {
        if (user) {
            res.status(422).json({ message: 'Duplicate email' });
        } else {
            hashPasswordAndRegisterUser(req, res);
        }
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

function validPassword(password) {
    if (password.length < 6 || password.length > 32) {
        return false;
    } else {
        return true;
    }
}

function hashPasswordAndRegisterUser(req, res) {
    bcrypt.hash(req.body.password, 10, (error, hash) => {
        if (error) {
            res.status(500).json({ error: error });
        } else {
            createUser(hash, req, res);
        }
    });
}

function createUser(hash, req, res) {
    const id = new mongoose.Types.ObjectId;
    const user = new User({
        _id: id,
        email: req.body.email,
        password: hash,
        username: 'User-' + id,
        imageUrl: 'uploads/profiles/profile.jpg',
        created: Date.now()
    });

    User.save(user, res);
}

exports.login = (req, res, next) => {
    User.findOne({
        email: req.body.email
    }).then(user => {
        if (user) {
            comparePasswords(user, req, res);
        } else {
            res.status(401).json({ message: 'Auth failed' });
        }
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

function comparePasswords(user, req, res) {
    bcrypt.compare(req.body.password, user.password, (error, authorized) => {
        if (error) {
            res.status(401).json({ message: 'Auth failed' });
        } else if (authorized) {
            returnToken(user, res);
        } else {
            res.status(401).json({ message: 'Auth failed' });
        }
    });
}

function returnToken(user, res) {
    const token = jwt.sign(
        { id: user._id },
        ENV_VARS.JWT_SECRET,
        { expiresIn: CONSTANTS.TOKEN_EXP }
    );

    res.status(200).json({
        token: token,
        active: user.active
    });
}

exports.changeUsername = (req, res, next) => {
    User.findOne({
        username: req.body.username
    }).then(user => {
        if (user) {
            res.status(422).json({ message: 'Cannot use this username' });
        } else {
            changeAccountUsername(req, res);
        }
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

function changeAccountUsername(req, res) {
    User.findById(req.userData.id).then(user => {
        user.username = req.body.username;
        User.save(user, res);
    }).catch(error => {
        res.status(500).json({ error: error });
    });
}

exports.changeFullname = (req, res, next) => {
    User.findById(req.userData.id).then(user => {
        user.fullname = req.body.fullname;
        User.save(user, res);
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

exports.changeBiogram = (req, res, next) => {
    User.findById(req.userData.id).then(user => {
        user.biogram = req.body.biogram;
        User.save(user, res);
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

exports.changePassword = (req, res, next) => {
    User.findById(req.userData.id).then(user => {
        compareOldPassword(user, req, res);
    })
};

function compareOldPassword(user, req, res) {
    bcrypt.compare(req.body.oldPassword, user.password, (error, authorized) => {
        if (error) {
            res.status(401).json({ message: 'Auth failed' });
        } else if (authorized) {
            hashNewPassword(user, req, res);
        } else {
            res.status(401).json({ message: 'Auth failed' });
        }
    });
}

function hashNewPassword(user, req, res) {
    bcrypt.hash(req.body.newPassword, 10, (error, hash) => {
        if (error) {
            res.status(500).json({ error: error });
        } else {
            user.password = hash;
            User.save(user, res);
        }
    });
}

exports.setActive = (req, res, next) => {
    User.findById(req.userData.id).then(user => {
        user.active = true;
        User.save(user, res);
    }).catch(error => {
        res.status(500).json({ error: error });
    });
};

exports.addActivityToProfile = (activity, res) => {
    User.findById(activity.author, 'activities').then(user => {
        if (user) {
            user.activities.push(activity);
            User.save(user, res);
        } else {
            res.status(201).json({ message: 'OK' });
        }
    }).catch(error => {
        res.status(500).json({ error: error });
    });
}
