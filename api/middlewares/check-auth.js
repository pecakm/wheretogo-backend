const jwt = require('jsonwebtoken');
const ENV_VARS = require('../../env_vars');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedData = jwt.verify(token, ENV_VARS.JWT_SECRET);
        req.userData = decodedData;
        next();
    } catch (error) {
        res.status(401).json({ message: 'Auth failed' });
    }
};
