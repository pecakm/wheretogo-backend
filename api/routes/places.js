const express = require('express');
const checkAuth = require('../middlewares/check-auth');
const PlacesController = require('../controllers/places-controller');

const router = express.Router();

router.get('/detail/user/:placeId', checkAuth, PlacesController.getDetailLogged);
router.get('/detail/:placeId', PlacesController.getDetail);
router.get('', PlacesController.getPlaces);
router.post('/search', PlacesController.findPlace);

module.exports = router;