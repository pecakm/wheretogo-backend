const express = require('express');
const ActivitiesController = require('../controllers/activities-controller');
const addImage = require('../middlewares/add-image');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();

router.get('/like/:activityId', checkAuth, ActivitiesController.like);
router.post('/add', addImage.single('activityImage'), ActivitiesController.addNew);

module.exports = router;
