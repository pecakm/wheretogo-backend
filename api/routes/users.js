const express = require('express');
const checkAuth = require('../middlewares/check-auth');
const UsersController = require('../controllers/users-controller');

const router = express.Router();

router.get('/my-account', checkAuth, UsersController.getMyAccount);
router.get('/:userId', checkAuth, UsersController.getUser);
router.post('/signup', UsersController.signup);
router.post('/login', UsersController.login);
router.put('/my-account/username', checkAuth, UsersController.changeUsername);
router.put('/my-account/fullname', checkAuth, UsersController.changeFullname);
router.put('/my-account/biogram', checkAuth, UsersController.changeBiogram);
router.put('/my-account/password', checkAuth, UsersController.changePassword);
router.put('/my-account/active/:userId', checkAuth, UsersController.setActive);

module.exports = router;
