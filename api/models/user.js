const mongoose = require('mongoose');
const Activity = require('./activity');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: {
        type: String,
        required: true
    },
    username: {
        type: String,
        trim: true,
        required: true
    },
    fullname: String,
    biogram: String,
    imageUrl: String,
    active: Boolean,
    created: Number
});

userSchema.statics.createMyAccountResponse = (user) => {
    const response = {
        id: user._id,
        email: user.email,
        username: user.username,
        fullname: user.fullname,
        biogram: user.biogram,
        imageUrl: user.imageUrl
    };

    return response;
}

userSchema.statics.createProfileResponse = (user, req) => {
    const response = {
        username: user.username,
        fullname: user.fullname,
        biogram: user.biogram,
        imageUrl: user.imageUrl,
        activities: Activity.createActivitiesResponse(user.activities, req)
    };

    return response;
}

userSchema.statics.save = (user, res) => {
    user.save().then(updatedUser => {
        res.status(201).json({ message: 'OK' });
    }).catch(error => {
        res.status(500).json({ error: error });
    });
}

module.exports = mongoose.model('User', userSchema);
