const mongoose = require('mongoose');

const activitySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    place: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Place'
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    date: Number,
    text: String,
    imageUrl: String,
    likes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});

activitySchema.statics.createActivitiesResponse = (activities, req) => {
    let response = [];

    for (activity of activities) {
        if (req.userData && activity.likes.indexOf(req.userData.id) >= 0) {
            const activityElement = {
                id: activity._id,
                likesCount: activity.likes.length,
                place: activity.place,
                date: activity.date,
                text: activity.text,
                author: activity.author,
                imageUrl: activity.imageUrl,
                userLiked: true
            };

            response.push(activityElement);
        } else {
            const activityElement = {
                id: activity._id,
                likesCount: activity.likes.length,
                place: activity.place,
                date: activity.date,
                text: activity.text,
                author: activity.author,
                imageUrl: activity.imageUrl
            };

            response.push(activityElement);
        }
    }

    return response;
}

module.exports = mongoose.model('Activity', activitySchema);
