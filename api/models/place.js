const mongoose = require('mongoose');
const Activity = require('./activity');

const placeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    location: {
        type: { lng: Number, lat: Number },
        index: '2dsphere',
        required: true
    },
    name: String,
    countryCode: String,
    postcode: String,
    state: String,
    locality: String,
    route: String,
    streetNumber: String,
    category: String,
    visits: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    activities: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Activity'
    }]
});

placeSchema.statics.markerInfo = () => {
    return {
        location: 1,
        name: 1,
        postcode: 1,
        locality: 1,
        route: 1,
        streetNumber: 1,
        category: 1,
        activitiesCount: { $size: '$activities' },
        distance: 1
    };
}

placeSchema.statics.createPlaceDetailResponse = (place, req) => {
    const response = {
        id: place._id,
        location: place.location,
        address: createAddressString(place),
        activities: Activity.createActivitiesResponse(place.activities, req)
    };

    return response;
}

function createAddressString(place) {
    let addressString = '';

    addressString = place.route ? addressString + place.route : addressString;
    addressString = place.streetNumber ? addressString + ' ' + place.streetNumber : addressString;
    addressString = place.route ? addressString + ', ' : addressString;
    addressString = place.locality ? addressString + place.locality : addressString;
    addressString = place.postcode && place.locality ? addressString + ' ' : addressString;
    addressString = place.postcode ? addressString + place.postcode : addressString;

    if (place.name) {
        return addressString != '' ? place.name + ` (${addressString})` : place.name;
    } else {
        return addressString;
    }
}

placeSchema.statics.createPlacesResponse = (places) => {
    let response = [];
    
    for (place of places) {
        const location = {
            id: place._id,
            location: place.location,
            address: createAddressString(place),
            category: place.category,
            activitiesCount: place.activitiesCount,
            distance: Math.round(place.distance * 10) / 10
        };

        response.push(location);
    }

    return response;
}

placeSchema.statics.createCurrentPlaceResponse = (place) => {
    const response = {
        id: place._id,
        location: place.location,
        address: createAddressString(place)
    };

    return response;
}

module.exports = mongoose.model('Place', placeSchema);
